<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'v1'], function () {
  Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'App\Http\Controllers\Api\Auth\AuthController@loginAuthController');
    Route::post('/register', 'App\Http\Controllers\Api\Auth\AuthController@registerAuthController');
    Route::post('/session', 'App\Http\Controllers\Api\Auth\AuthController@refreshTokenAuthController');
    Route::post('/logout', 'App\Http\Controllers\Api\Auth\AuthController@logoutAuthController')->middleware('logout.jwt');
  });
  Route::group(['prefix' => 'wallets'], function () {
    Route::get('/list', 'App\Http\Controllers\Api\Wallets\WalletsController@listWalletsController');
    Route::post('/transfer', 'App\Http\Controllers\Api\Wallets\WalletsController@transferWalletsController');
    Route::post('/topup', 'App\Http\Controllers\Api\Wallets\WalletsController@topUpWalletsController');
  })->middleware('check.jwt');
  Route::group(['prefix' => 'transactions'], function () {
    Route::get('/list', 'App\Http\Controllers\Api\Transactions\TransactionsController@listTransactionsController');
  });
});