<?php
namespace App\Helper;
use Illuminate\Support\Facades\Hash;
class AuthHelper {
  public static function passwordCompare($password_db, $password_req){
    $compare = Hash::check($password_req, $password_db);
    if($compare) return true;
    return false;
  }

  public static function passwordHash($password){
    return Hash::make($password);
  }

}