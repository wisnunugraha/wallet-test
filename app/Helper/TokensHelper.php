<?php
namespace App\Helper;
use Illuminate\Support\Facades\Hash;
use App\Repository\TokensRepository;
use App\Repository\AuthRepository;
use JWTAuth;
class TokensHelper {
  private $tokensRepository;
  public function __construct(TokensRepository $tokensRepository, AuthRepository $authRepository)
  {
      $this->tokensRepository = $tokensRepository;
      $this->authRepository = $authRepository;
  }

  public function tokensFind($params){
    return $this->tokensRepository->findTokensRepository($params);
  }

  public function tokensRefresh($token){
    $tokens = $this->tokensRepository->findTokensRepository(['token' => $token]);
    if($tokens['status'] === true){
      $user = $this->authRepository->findAuthRepository(['id' => $tokens['response']->auths_id]);
      // dd($user);
        $users = $user['response'];
        // dd($users);
        // Generate Token and Refresh Token
        $tokenJWT = JWTAuth::fromUser($users);
        $tokenRefresh = JWTAuth::fromUser($users);
        $response = [
          'token' => [
              'access_token' => $tokenJWT,
              'refresh_token' => $tokenRefresh
          ]
      ];
      return $response;
    }
  }
  public function tokensInsert($id, $token){
    $check = $this->tokensRepository->findTokensRepository(['auths_id' => $id]);
    if ($check['status'] === false) {
        return $this->tokensRepository->insertTokensRepository([
            'auths_id' => $id,
            'token' => $token,
        ]);
    } else {
        return $this->tokensRepository->updateTokensRepository($id, [
            'token' => $token,
        ]);
    }
}
  public function tokensUpdate($token_old, $token_new){
    $check = $this->tokensRepository->findTokensRepository(['token' => $token_old]);
    if($check['status'] === true){
      $this->tokensRepository->updateTokensRepository($check['response']->id, [
        'token' => $token_new
      ]);
    }
    
  }

}