<?php
namespace App\Repository;
use App\Models\Auths;
use Carbon\Carbon;

class AuthRepository {
  public static function findAuthRepository($params){

    // Define Model
    $query = Auths::query();
    // Conditions Search
    if (isset($params['id'])) $query->where('id', $params['id']);
    if (isset($params['email'])) $query->where('email', $params['email']);
    // Execution Query
    $response = $query->whereNull('deleted_at')->first();
    // Response Query
    if(isset($response)) return ['status'=> true, 'response' => $response];
    return ['status'=> false, 'response' => null, 'message'=>'Data users not found'];

  }

  public static function insertAuthRepository($params){
    $params['created_at'] = Carbon::now();
    // Define Model and Executions
    $query = Auths::create($params);
    return $query;
  }

  public static function updateAuthRepository($id, $params){
    $params['updated_at'] = Carbon::now();
    
    // Define Model
    $query = Auths::query();
    // Conditions Search
    if (isset($params['id'])) $query->where('id', $params['id']);
    if (isset($params['email'])) $query->where('email', $params['email']);
    // Execution Query
    $query->whereNull('deleted_at');
    $query->update($params);
    return $query;
  }

  public static function deleteAuthRepository($params){
    $query = Auths::where('id', $params['id'])->update(['deleted_at' => Carbon::now()]);
    return $query;
  }
}