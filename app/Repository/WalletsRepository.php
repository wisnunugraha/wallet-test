<?php
namespace App\Repository;

use App\Models\Wallets;
use Carbon\Carbon;

class WalletsRepository
{
  public static function findWalletsRepository($params)
  {
    // dd($params["users_id"]);
    $query = Wallets::query();
    if (isset($params['id'])) {
      $query->where('id', $params['id']);
    }

    if (isset($params['users_id'])) {
      $query->where('users_id', $params['users_id']);
    }

    if (isset($params['account'])) {
      $query->where('account', $params['account']);
    }

    $response = $query->whereNull('deleted_at')->first();
    return $response;
  }

  public static function listWalletsRepository($limit)
  {
    $query = Wallets::whereNull('deleted_at')->paginate($limit);
    return $query;
  }


  public static function insertWalletsRepository($params)
  {
    $params['created_at'] = Carbon::now();
    $query = Wallets::create($params);
    return $query;
  }

  public static function updateWalletsRepository($id, $params)
  {
    $params['updated_at'] = Carbon::now();
    $query = Wallets::where('users_id', $id)->update($params);
    return $query;
  }
  public static function incrementWalletsRepository($id, $amount)
  {
      // $params['updated_at'] = Carbon::now();
      $query = Wallets::where('users_id', $id)->increment('balance', $amount);
      return $query;
  }
  
  public static function decrementWalletsRepository($id, $amount)
  {
      // $params['updated_at'] = Carbon::now();
      $query = Wallets::where('users_id', $id)->decrement('balance', $amount);
      return $query;
  }

  public static function deleteWalletsRepository($id)
  {
    $query = Wallets::where('id', $id)->update(['deleted_at' => Carbon::now()]);
    return $query;
  }
}