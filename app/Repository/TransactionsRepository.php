<?php
namespace App\Repository;

use App\Models\Transactions;
use Carbon\Carbon;

class TransactionsRepository
{
  public static function listTransactionsRepository($params)
  {

    // Define Model
    $query = Transactions::query();
    // Conditions Search
    if (isset($params['id']))
      $query->where('id', $params['id']);
    if (isset($params['users_id']))
      $query->where('users_id', $params['users_id']);
    if (isset($params['refrence_id']))
      $query->where('refrence_id', $params['refrence_id']);
    if (isset($params['type']))
      $query->where('type', $params['type']);
    if (isset($params['status']))
      $query->where('status', $params['status']);
    // Execution Query
    $response = $query->get();
    // Response Query
    return $response;

  }

  public static function insertTransactionsRepository($params)
  {
    // dd($params['token']);
    $params['created_at'] = Carbon::now();
    // Define Model and Executions
    $query = Transactions::create($params);
    return $query;
  }

  public static function updateTransactionsRepository($id, $params)
  {
    // Define Model
    $query = Transactions::where('auths_id', $id)->update(['token' => $params['token'], 'updated_at' => Carbon::now()]);
    return $query;
  }

  public static function deleteTransactionsRepository($params)
  {
    $query = Transactions::query();
    // Conditions Search
    if (isset($params['id']))
      $query->where('id', $params['id']);
    if (isset($params['token']))
      $query->where('token', $params['token']);
    // Execution Query
    $response = $query->delete();
    return $response;
  }
}