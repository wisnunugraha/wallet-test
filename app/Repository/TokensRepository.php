<?php
namespace App\Repository;
use App\Models\Tokens;
use Carbon\Carbon;

class TokensRepository {
  public static function findTokensRepository($params){

    // Define Model
    $query = Tokens::query();
    // Conditions Search
    if (isset($params['auths_id'])) $query->where('auths_id', $params['auths_id']);
    if (isset($params['token'])) $query->where('token', $params['token']);
    // Execution Query
    $response = $query->first();
    // Response Query
    if(isset($response)) return ['status'=> true, 'response' => $response];
    return ['status'=> false, 'response' => null, 'message'=>'Tokens not found'];

  }

  public static function insertTokensRepository($params){
    // dd($params['token']);
    $params['created_at'] = Carbon::now();
    // Define Model and Executions
    $query = Tokens::create(['auths_id'=>$params['auths_id'], 'token' =>$params['token'], 'created_at'=>Carbon::now()]);
    return $query;
  }

  public static function updateTokensRepository($id, $params){
    // Define Model
    $query = Tokens::where('auths_id', $id)->update(['token'=>$params['token'],'updated_at'=>Carbon::now()]);
    return $query;
  }

  public static function deleteTokensRepository($params){
    $query = Tokens::query();
    // Conditions Search
    if (isset($params['id'])) $query->where('id', $params['id']);
    if (isset($params['token'])) $query->where('token', $params['token']);
    // Execution Query
    $response = $query->delete();
    return $response;
  }
}