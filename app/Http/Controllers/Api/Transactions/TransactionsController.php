<?php

namespace App\Http\Controllers\Api\Transactions;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Repository\WalletsRepository;
use App\Repository\TransactionsRepository;
use App\Repository\AuthRepository;
use App\Utils\ResponseJson;
use App\Utils\ResponseCommon;

use Carbon\Carbon;

class TransactionsController extends Controller
{

  private $transactionRepository;
  private $authRepository;
  private $responseJson;
  private $responseCommon;
  public function __construct(AuthRepository $authRepository, TransactionsRepository $transactionRepository, ResponseJson $responseJson, ResponseCommon $responseCommon)
  {
    $this->transactionRepository = $transactionRepository;
    $this->authRepository = $authRepository;
    $this->responseJson = $responseJson;
    $this->responseCommon = $responseCommon;
  }
  public function listTransactionsController(Request $request)
  {
    // Validation data need
    $validator = Validator::make($request->all(), [
      'id' => 'integer',
      'users_id' => 'integer',
      'reference_id' => 'integer',
      'date' => 'string',
      'type' => 'integer',
      'status' => 'integer',
    ]);
    // Validtaion check
    if ($validator->fails())
      return $this->responseCommon->BadResponseCommon($validator->errors());

    $data = $this->transactionRepository->listTransactionsRepository($request->all());
    // dd($data);
    if (!empty($data)) {
      foreach ($data as $transaction) {
        // dd($transaction->id);
        switch ($transaction->type) {
          case 1:
            $transaction->type_name = 'Transfer';
            break;
          case 2:
            $transaction->type_name = 'Receive';
            break;
          case 3:
            $transaction->type_name = 'Top Up';
            break;
        }
      }

      return $this->responseJson->successResponseJson($data, 'wallets updated successfully');
    }

    return $this->responseCommon->badResponseCommon('data null on list success');




  }
}

