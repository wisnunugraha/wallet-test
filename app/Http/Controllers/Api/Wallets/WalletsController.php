<?php

namespace App\Http\Controllers\Api\Wallets;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Repository\WalletsRepository;
use App\Repository\TransactionsRepository;
use App\Repository\AuthRepository;
use App\Utils\ResponseJson;
use App\Utils\ResponseCommon;

use Carbon\Carbon;

class WalletsController extends Controller
{

  private $transactionRepository;
  private $authRepository;
  private $walletsRepository;
  private $responseJson;
  private $responseCommon;
  public function __construct(WalletsRepository $walletsRepository, AuthRepository $authRepository, TransactionsRepository $transactionRepository, ResponseJson $responseJson, ResponseCommon $responseCommon)
  {
    $this->transactionRepository = $transactionRepository;
    $this->authRepository = $authRepository;
    $this->walletsRepository = $walletsRepository;
    $this->responseJson = $responseJson;
    $this->responseCommon = $responseCommon;
  }
  public function topUpWalletsController(Request $request)
  {
    // Validation data need
    $validator = Validator::make($request->all(), [
      'amount' => 'required|integer',
    ]);
    // Validtaion check
    if ($validator->fails())
      return $this->responseCommon->BadResponseCommon($validator->errors());
    if ($request->amount <= 0) {
      return $this->responseCommon->BadResponseCommon('amount must be greater than 0');
    }

    $users_id = $this->authRepository->findAuthRepository(['id' => $request->id]);
    if (!$users_id) {
      return $this->responseCommon->BadResponseCommon("account does not exist");
    }

    $walletsUsers = $this->walletsRepository->findwalletsRepository(['users_id' => $request->id]);
    if (!$walletsUsers) {
      return $this->responseCommon->BadResponseCommon("wallets account does not exist");
    }


    $transactionsUsers = [
      "users_id" => $request->id,
      "reference_id" => null,
      "balance_before" => $walletsUsers->balance,
      "balance_after" => $walletsUsers->balance + $request->amount,
      "credit" => $request->amount,
      "debit" => 0,
      "date" => Carbon::now()->format("y-m-d"),
      "type" => 3,
      "status" => 1,
      "created_at" => Carbon::now()
    ];
    $this->transactionRepository->insertTransactionsRepository($transactionsUsers);
    // Update wallets
    $wallets = $this->walletsRepository->incrementWalletsRepository($request->id, $request->amount);

    // Return the response
    return $this->responseJson->successResponseJson($wallets, 'wallets updated successfully');
  }
  public function transferWalletsController(Request $request)
  {
    // Validation data need
    $validator = Validator::make($request->all(), [
      'users_id' => 'required|string',
      'account_to' => 'required|integer',
      'amount_to' => 'required|integer',
    ]);
    // Validtaion check
    if ($validator->fails())
      return $this->responseCommon->BadResponseCommon($validator->errors());

    $users_id = $this->authRepository->findAuthRepository(['id' => $request->users_id]);
    if (!$users_id) {
      return $this->responseCommon->BadResponseCommon("account does not exist");
    }

    $walletsUsers = $this->walletsRepository->findwalletsRepository(['users_id' => $request->users_id]);
    if (!$walletsUsers) {
      return $this->responseCommon->BadResponseCommon("wallets account does not exist");
    }

    $walletsTo = $this->walletsRepository->findwalletsRepository(['account' => $request->account_to]);
    if (!$walletsTo) {
      return $this->responseCommon->BadResponseCommon("wallets account transfer does not exist");
    }



    $transactionsUsers = [
      "users_id" => $request->users_id,
      "reference_id" => $walletsTo->users_id,
      "balance_before" => $walletsUsers->balance,
      "balance_after" => $walletsUsers->balance - $request->amount_to,
      "credit" => 0,
      "debit" => $request->amount_to,
      "date" => Carbon::now()->format("y-m-d"),
      "type" => 1,
      "status" => 1,
      "created_at" => Carbon::now()
    ];

    $transactionsToUsers = [
      "users_id" => $walletsTo->users_id,
      "reference_id" => $request->users_id,
      "balance_before" => $walletsTo->balance,
      "balance_after" => $walletsTo->balance + $request->amount_to,
      "credit" => $request->amount_to,
      "debit" => 0,
      "date" => Carbon::now()->format("y-m-d"),
      "type" => 2,
      "status" => 1,
      "created_at" => Carbon::now()
    ];

    $this->transactionRepository->insertTransactionsRepository($transactionsUsers);
    $this->transactionRepository->insertTransactionsRepository($transactionsToUsers);
    $this->walletsRepository->decrementWalletsRepository($request->users_id, $request->amount_to);
    $this->walletsRepository->incrementWalletsRepository($walletsTo->users_id, $request->amount_to);


    // Return the response
    return $this->responseJson->successResponseJson(null, 'wallets transfer successfully');
  }

  public function deleteWalletsController(Request $request)
  {
    // Validation data need
    $validator = Validator::make($request->all(), [
      'id' => 'required|integer',
    ]);
    // Validtaion check
    if ($validator->fails())
      return $this->responseCommon->BadResponseCommon($validator->errors());

    // Delete wallets
    $wallets = $this->walletsRepository->deletewalletsRepository($request->id);

    // Return the response
    return $this->responseJson->successResponseJson($wallets, 'wallets deleted successfully');

  }
}

