<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Repository\AuthRepository;
use App\Repository\WalletsRepository;
use App\Utils\ResponseJson;
use App\Utils\ResponseCommon;
use App\Helper\AuthHelper;
use App\Helper\TokensHelper;

use Session;
use JWTAuth;


class AuthController extends Controller
{
    private $walletsRepository;
    private $authRepository;
    private $responseJson;
    private $responseCommon;
    private $authHelper;
    private $tokensHelper;
    public function __construct(AuthRepository $authRepository, WalletsRepository $walletsRepository, ResponseJson $responseJson, ResponseCommon $responseCommon, TokensHelper $tokensHelper, AuthHelper $authHelper)
    {
        $this->walletsRepository = $walletsRepository;
        $this->authRepository = $authRepository;
        $this->responseJson = $responseJson;
        $this->responseCommon = $responseCommon;
        $this->authHelper = $authHelper;
        $this->tokensHelper = $tokensHelper;
    }


    public function loginAuthController(Request $request)
    {
        // Validations data need
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:100',
            'password' => 'required',
        ]);

        // Validtaion check
        if ($validator->fails())
            return $this->responseCommon->BadResponseCommon($validator->errors());

        // Find Email First
        $result = $this->authRepository->findAuthRepository(['email' => $request->email]);
        if ($result['status'] === false)
            return $this->responseCommon->BadResponseCommon($result['message']);

        // Compare Password
        $compare = $this->authHelper->passwordCompare($result['response']->password, $request->password);
        if ($compare === false)
            return $this->responseCommon->BadResponseCommon('invalid email or password');

        // Session data user
        $session = [
            'id' => $result['response']->id,
            'fname' => $result['response']->fname,
            'lname' => $result['response']->lname,
            'email' => $result['response']->email,
        ];

        // Session data user
        $user = $result['response'];
        Session::put($result['response']->id, $session);

        // Generate Token and Refresh Token
        $tokenJWT = JWTAuth::fromUser($user);
        $tokenRefresh = JWTAuth::fromUser($user);
        $this->tokensHelper->tokensInsert($result['response']->id, $tokenRefresh);
        // Genereate Token and Refresh Token
        $response = [
            'user' => $result['response'],
            'token' => [
                'access_token' => $tokenJWT,
                'refresh_token' => $tokenRefresh
            ]
        ];

        // Execution response
        return $this->responseJson->successResponseJson($response, 'login success');

    }

    public function registerAuthController(Request $request)
    {
        // Validation data need
        $validator = Validator::make($request->all(), [
            'fname' => 'required|string|between:2,100',
            'lname' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100',
            'password' => 'required|string|min:6',
        ]);
        // Validtaion check
        if ($validator->fails())
            return $this->responseCommon->BadResponseCommon($validator->errors());
        // Find Email
        $result = $this->authRepository->findAuthRepository(['email' => $request->email]);
        if ($result['status'] === true)
            return $this->responseCommon->BadResponseCommon('Email already exists');

        // Hash Password
        $password = $this->authHelper->passwordHash($request->password);

        // Create Password
        $user = $this->authRepository->insertAuthRepository([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'password' => $password,
        ]);

        $wallets = $this->walletsRepository->insertWalletsRepository([
            'users_id' => $user->id,
            'account' => '1000200' + $user->id,
            'balance' => 0,
        ]);

        // Session::put($result['response']->id, $user);
        $tokenJWT = JWTAuth::fromUser($user);
        $tokenRefresh = JWTAuth::fromUser($user);
        $this->tokensHelper->tokensInsert($user->id, $tokenRefresh);

        // Genereate Token and Refresh Token
        $response = [
            'user' => $user,
            'wallet' => $wallets,
            'token' => [
                'access_token' => $tokenJWT,
                'refresh_token' => $tokenRefresh
            ]
        ];

        return $this->responseJson->successResponseJson($response, 'register success');

    }

    public function refreshTokenAuthController(Request $request)
    {
        $authorizationHeader = $request->header('Authorization');

        // Extract the token from the Authorization header
        $token = str_replace('Bearer ', '', $authorizationHeader);
        $tokens = $this->tokensHelper->tokensRefresh(['token' => $token]);
        // dd($tokens);
        if (isset($tokens))
            return $this->responseJson->successResponseJson($tokens, 'Token refreshed created');
        return $this->responseCommon->BadResponseCommon('Token not found');

    }

    public function logoutAuthController(Request $request)
    {
        return $this->responseJson->successResponseJson(null, 'Success logout');
    }

}
