<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helper\TokensHelper;
use App\Utils\ResponseJson;

class CheckJwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */

    private $tokensHelper;
    private $responseJson;
    public function __construct(TokensHelper $tokensHelper, ResponseJson $responseJson)
    {
        $this->tokensHelper = $tokensHelper;
        $this->responseJson = $responseJson;
    }

    public function handle(Request $request, Closure $next): Response
    {
        try {
            $token = JWTAuth::parseToken();
            $payload = JWTAuth::getPayload($token)->toArray();


            if (!$payload) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $next($request);
    }
}
