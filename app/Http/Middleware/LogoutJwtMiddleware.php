<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class LogoutJwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        try {
            // Get the token from the request
            $token = JWTAuth::getToken();

            // Check if the token is blacklisted
            JWTAuth::setToken($token)->invalidate();
            return response()->json(['error' => 'Token has been revoked'], 401);
        } catch (\Exception $e) {
            // Handle exceptions (e.g., token not provided)
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $next($request);
    }
}
