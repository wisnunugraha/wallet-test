<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
  use HasFactory;
  protected $table = 'transactions';
  protected $fillable = [
    'users_id',
    'reference_id',
    'balance_before',
    'balance_after',
    'credit',
    'debit',
    'date',
    'type',
    'status',
    'created_at',
    'updated_at',
  ];
}
