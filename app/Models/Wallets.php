<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallets extends Model
{
  use HasFactory;
  protected $table = 'wallets';
  protected $fillable = [
    'users_id',
    'account',
    'balance',
    'type',
    'status',
    'created_at',
    'updated_at',
    'deleted_at',
  ];
}
