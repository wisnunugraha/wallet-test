<?php
namespace App\Utils;

class ResponseJson {
  public static function successResponseJson($response , $message){
    return response()->json(['ok'=> true, 'data'=> $response, 'message'=> $message],200);
  }
}
