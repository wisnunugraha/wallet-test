<?php
namespace App\Utils;

class ResponseCommon {
  public static function BadResponseCommon($message){
    return response()->json([
      'ok' => false,
      "err" => 'ERR_BAD_REQUEST',
      "msg" => $message
    ],400);
  }

  public static function UnauthorizedResponseCommon(){
    return response()->json([
      'ok' => false,
      "err" => 'ERR_BAD_REQUEST',
      "msg" => "invalid access token"
    ], 401);
  }

  public static function ForbiddenResponseCommon(){
    return response()->json([
      'ok' => false,
      "err" => 'ERR_BAD_REQUEST',
      "msg" => `user doesn't have enough authorization`
    ], 403);
  }

  public static function NotFoundResponseCommon(){
    return response()->json([
      'ok' => false,
      "err" => 'ERR_BAD_REQUEST',
      "msg" => 'resource is not found'
    ],404);
  }


  public static function ServerErrorFoundResponseCommon($message){
    return response()->json([
      'ok' => false,
      "err" => 'ERR_BAD_REQUEST',
      "msg" => 'unable to connect into database'
    ],500);
  }
}