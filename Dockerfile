# Use the official PHP image with FPM
FROM php:8.3-fpm

# Install dependencies
RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql zip

# Set the working directory
WORKDIR /var/www/html

# Copy the project files to the container
COPY . .

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install dependencies
RUN composer install

# Set permissions
RUN chown -R www-data:www-data storage bootstrap/cache

RUN php artisan key:generate &&  php artisan jwt:secret && php artisan optimize:clear
# Expose port 9000 for FPM
EXPOSE 9000

CMD ["php-fpm"]
