@extends('Layout.authLayout')
@section('content')
<header>
    <style>
      #intro {
        background-image: url(https://mdbootstrap.com/img/new/fluid/city/008.jpg);
        height: 100vh;
      }

      /* Height for devices larger than 576px */
      @media (min-width: 992px) {
        #intro {
          margin-top: -58.59px;
        }
      }

      .navbar .nav-link {
        color: #fff !important;
      }
    </style>

    <!-- Background image -->
    <div id="intro" class="bg-image shadow-2-strong">
      <div class="mask d-flex align-items-center h-100" style="background-color: rgba(0, 0, 0, 0.8);">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-md-8">
              <form class="bg-white rounded shadow-5-strong p-5" id='loginForm'>
                <!-- Email input -->
                <div class="form-outline mb-4">
                  <input type="email" name="email" id="form1Example1" class="form-control" placeholder="Email" />
                  <label class="form-label" for="form1Example1">Email address</label>
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                  <input type="password" name="password" id="form1Example2" class="form-control" placeholder="Password" />
                  <label class="form-label" for="form1Example2">Password</label>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Background image -->
  </header>
  <!--Main Navigation-->


    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2020 Copyright:
      <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    $(document).ready(function () {
        $('#loginForm').submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: '/api/v1/auth/login',
                data: $(this).serialize(),
                success: function (response) {
                  if(response.ok === true){
                    localStorage.setItem('access_token', response.data.token.access_token);
                    localStorage.setItem('refresh_token', response.data.token.refresh_token);
                    localStorage.setItem('user', JSON.stringify(response.data.user));
                    localStorage.setItem('status_login', true);
                    window.location.href = '/reminders/list';
                  }
                },

                error: function(xhr, status, error) {
                  const data = JSON.parse(xhr.responseText);
                  if(data.ok === false){
                    alert(data.msg)
                    $('#form1Example1').val('');
                    $('#form1Example2').val('');
                  }
                }
            });
        });
    });
</script>
@endsection