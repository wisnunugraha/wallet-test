<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- MDB -->
    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/7.1.0/mdb.umd.min.js"
    ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles
    <link href="{{ asset('src/css/mdb.min.css') }}" rel="stylesheet"> -->
    <!-- {{ asset('css/app.css') }} -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/7.1.0/mdb.min.css" rel="stylesheet"/>
    <style>
        .table-data {
            font-size:12px;
        }
    </style>
</head>
<body>
    <div id="app">
        <main >

            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-dark d-none d-lg-block" style="z-index: 2000;">
              <div class="container-fluid">
                <!-- Navbar brand -->
                <a class="navbar-brand nav-link" target="_blank" href="https://mdbootstrap.com/docs/standard/">
                  <strong>Reminders</strong>
                </a>
                <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarExample01"
                  aria-controls="navbarExample01" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarExample01">
                  <ul class="navbar-nav me-auto mb-2 mb-lg-0" id="navLinksLoggedIn" style="display: none;">
                      <li class="nav-item">
                          <a class="nav-link" href="/reminders/list">Dashboard</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" id='logoutButton'>Logout</a>
                      </li>
                  </ul>
                  <ul class="navbar-nav me-auto mb-2 mb-lg-0" id="navLinksLoggedOut" style="display: none;">
                      <li class="nav-item active">
                          <a class="nav-link" aria-current="page" href="/">Login</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="/register">Register</a>
                      </li>
                  </ul>

                  <ul class="navbar-nav d-flex flex-row">
                    <!-- Icons -->
                    <li class="nav-item me-3 me-lg-0">
                      <a class="nav-link" href="https://www.youtube.com/channel/UC5CF7mLQZhvx8O5GODZAhdA" rel="nofollow"
                        target="_blank">
                        <i class="fab fa-youtube"></i>
                      </a>
                    </li>
                    <li class="nav-item me-3 me-lg-0">
                      <a class="nav-link" href="https://www.facebook.com/mdbootstrap" rel="nofollow" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                      </a>
                    </li>
                    <li class="nav-item me-3 me-lg-0">
                      <a class="nav-link" href="https://twitter.com/MDBootstrap" rel="nofollow" target="_blank">
                        <i class="fab fa-twitter"></i>
                      </a>
                    </li>
                    <li class="nav-item me-3 me-lg-0">
                      <a class="nav-link" href="https://github.com/mdbootstrap/mdb-ui-kit" rel="nofollow" target="_blank">
                        <i class="fab fa-github"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            <!-- Navbar -->

            @yield('content')
        </main>
    </div>
<script>
    $(document).ready(function () {
        // Check the status_login in localStorage
        var statusLogin = localStorage.getItem('status_login');

        var token = localStorage.getItem('access_token');
        console.log(statusLogin);
        // Show/hide navigation links based on status_login
        if (statusLogin === 'true') {
            $('#navLinksLoggedIn').show();
        } else {
            $('#navLinksLoggedOut').show();
        }


        $('#logoutButton').on('click',function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: '/api/v1/auth/logout',
                data: null,
                headers: {
                    'Authorization': 'Bearer ' + token, // Add the Authorization header with the token
                },
                success: function (response) {
                      localStorage.clear();
                      window.location.href = '/';
                },
                error: function (xhr, status, error) {
                      localStorage.clear();
                      window.location.href = '/';
                },
            });
        });


    });
</script>
</body>
</html>
