@extends('Layout.remindersLayout')

@section('content')

<div class="container mt-5">
    <div class="row justify-content-center">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light mb-4 bg-body-tertiary">
        <!-- Container wrapper -->
        <div class="container">
            <!-- Navbar brand -->

            <!-- Toggle button -->
            <button
            data-mdb-collapse-init
            class="navbar-toggler"
            type="button"
            data-mdb-target="#navbarButtonsExample"
            aria-controls="navbarButtonsExample"
            aria-expanded="false"
            aria-label="Toggle navigation"
            >
            <i class="fas fa-bars"></i>
            </button>

            <!-- Collapsible wrapper -->
            <div class="collapse navbar-collapse" id="navbarButtonsExample">
            <!-- Left links -->
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            </ul>
            <!-- Left links -->

            <div class="d-flex align-items-center">
                
                <button class="btn btn-primary" data-bs-toggle="modal" data-mdb-ripple-init data-mdb-modal-init data-mdb-target="#addReminders" id="btnAdd" data-mdb-ripple-init type="button" class="btn btn-primary me-3">
                Add Reminders
                </button>
            </div>
            </div>
            <!-- Collapsible wrapper -->
        </div>
        <!-- Container wrapper -->
        </nav>
        <!-- Navbar -->
        <table class="table align-middle mb-5 bg-white" id='reminders-table'>
            <thead class="bg-light">
                <tr>
                    <th>id</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Reminder At</th>
                    <th>Event At</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal  Add-->
<div class="modal" id="addReminders" tabindex="-1" aria-labelledby="addRemindersLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addRemindersLabel">Add Reminder</h5>
                <button type="button" class="btn-close" id="closeAddReminders" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Your modal content goes here -->

              <form class="bg-white p-3" id='addRemindersForm'>
                <div class="form mb-1">
                    <label class="form-label" for="formTitle">Title</label>
                  <input type="input" name="title" id="formTitle" class="form-control" placeholder="Title" require />
                </div>

                <div class="form mb-1">
                  <label class="form-label" for="formDescription">Description</label>
                  <input type="input" name="description" id="formDescription" class="form-control" placeholder="Description" require />
                </div>

                <div class="form mb-1">
                  <label class="form-label" for="formEvent">Event At</label>
                  <input type="datetime-local" name="event_at" id="formEvent" class="form-control" placeholder="Event Date" require />
                </div>
                <div class="form mb-1">
                    <button type="submit" class="btn btn-success" id="addRemindersSubmit" >Add Reminder</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<!-- Modal  Ubah-->
<div class="modal" id="editReminders" tabindex="-1" aria-labelledby="editRemindersLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editRemindersLabel">Edit Reminder</h5>
                <button type="button" class="btn-close" id="closeEditReminders" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form class="bg-white p-3" id='editRemindersForm'>
                <input type="text" hidden name="id" id="formEditId" class="form-control" placeholder="Title" require />
                <div class="form mb-1">
                    <label class="form-label" for="form1Example1">Title</label>
                  <input type="input" name="email" id="formEditTitle" class="form-control" placeholder="Email" />
                </div>

                <div class="form mb-1">
                  <label class="form-label" for="form1Example2">Description</label>
                  <input type="input" name="description" id="formEditDescription" class="form-control" placeholder="Password" />
                </div>

                <div class="form mb-1">
                  <label class="form-label" for="form1Example3">Event At</label>
                  <input type="datetime-local" name="event_at" id="formEditEvent" class="form-control" placeholder="Password" />
                </div>
                <div class="form mb-1">
                    <button type="submit" class="btn btn-success" id="editRemindersSubmit" >Add Reminder</button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal  Hapus-->
<div class="modal" id="deleteReminders" tabindex="-1" aria-labelledby="deleteRemindersLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteRemindersLabel">Delete Reminder</h5>
                <button type="button" class="btn-close" id="closeDeleteReminders" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Your modal content goes here -->

              <form class="bg-white p-3" id='deleteRemindersForm'>
                <input type="text" name="id" id="formDeleteId" hidden>
                <!-- Email input -->
                <div class="form mb-1">
                    <label class="form-label" for="form1Title">Title</label>
                  <input type="input" name="title" id="form1Title" class="form-control" placeholder="Title" readonly disabled />
                </div>
                <div class="form mb-1">
                  <label class="form-label" for="form1Example3">Event At</label>
                  <input type="datetime-local" name="event_at" id="form1Event" class="form-control" placeholder="Password" readonly disabled />
                </div>
                <div class="form mb-1">
                    <button type="submit" class="btn btn-danger " id="deleteRemindersSubmit" >Delete Reminder</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-danger">Save changes</button> -->
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        // Check the status_login in localStorage
        var statusLogin = localStorage.getItem('status_login');
        var token = localStorage.getItem('refresh_token');

        console.log(statusLogin);
        // Show/hide navigation links based on status_login
        if (statusLogin === 'true') {
            $('#navLinksLoggedIn').show();
        } else {
            window.location.href = '/';
        }
        // Fetch reminders data using AJAX
        $.ajax({
            url: "/api/v1/reminders/list", // Replace with your API route
            type: "GET",
            dataType: "json",
            success: function (data) {
                // Check if the response is successful
                if (data.ok) {
                    // Initialize DataTable
                    var table = $('#reminders-table').DataTable({
                                    data: data.data.reminders.data,
                                    columns: [
                                        { data: 'id' },
                                        { data: 'title' },
                                        { data: 'description' },
                                        { data: 'remind_at' },
                                        { data: 'event_at' },
                                        { data: 'created_at' }
                                    ],
                                    columnDefs: [
                                        {
                                            targets: 3,
                                            class: "text-center",
                                            width: '15%',
                                            data: 'id',
                                            render: function (data, type, row, meta) {
                                                // Modify this line to include the actual data you want to pass to the modal
                                                return new Date(data * 1000).toISOString().slice(0, 16);;
                                            }
                                        },
                                        {
                                            targets: 4,
                                            class: "text-center",
                                            width: '15%',
                                            data: 'id',
                                            render: function (data, type, row, meta) {
                                                // Modify this line to include the actual data you want to pass to the modal
                                                return new Date(data * 1000).toISOString().slice(0, 16);;
                                            }
                                        },
                                        {
                                            targets: 6,
                                            class: "text-center",
                                            width: '15%',
                                            data: 'id',
                                            render: function (data, type, row, meta) {
                                                // Modify this line to include the actual data you want to pass to the modal
                                                return '<button type="button" class="btn btn-warning" data-bs-toggle="modal" data-mdb-ripple-init data-mdb-modal-init data-mdb-target="#editReminders" id="btnUbah">Edit Reminders</button> <br><br>' +
                                                '<button type="button" class="btn btn-danger" data-bs-toggle="modal" data-mdb-ripple-init data-mdb-modal-init data-mdb-target="#deleteReminders" id="btnDelete">Delete Reminders</button>';
                                            }
                                        },
                                    ]
                                });

                    $('#reminders-table tbody').on('click','#btnUbah', function(){
                        $('#formEditTitle').val('')
                        $('#formEditDescription').val('')
                        $('#formEditEvent').val('')
                        $('#formEditId').val('');

                        var data = table.row($(this).parents('tr')).data();
                        var date = new Date(data.event_at * 1000).toISOString().slice(0, 16);
                        $('#formEditId').val(data.id);
                        $('#formEditTitle').val(data.title)
                        $('#formEditDescription').val(data.description)
                        $('#formEditEvent').val(date)
                        $('.modal-backdrop').addClass('show');
                        $('#editReminders').addClass('show').css('display', 'block');
                   
                    });
                    $('#closeEditReminders').on('click', function(){
                        $('#editReminders').removeClass('show').css('display', 'none');
                        $('.modal-backdrop').removeClass('.show').removeClass('modal-backdrop');
                    })



                    $('#reminders-table tbody').on('click','#btnDelete', function(){
                        $('#form1Title').val('')
                        $('#formDeleteId').val('')
                        $('#form1Event').val('')
                        var data = table.row($(this).parents('tr')).data();
                        var date = new Date(data.event_at * 1000).toISOString().slice(0, 16);
                        $('#formId').val(data.id);
                        $('.modal-backdrop').addClass('show');
                        $('#deleteReminders').addClass('show').css('display', 'block');
                        $('#formDeleteId').val(data.id)
                        $('#form1Title').val(data.title)
                        $('#form1Event').val(date)
                   
                    });
                    $('#closeDeleteReminders').on('click', function(){
                        $('#deleteReminders').removeClass('show').css('display', 'none');
                        $('.modal-backdrop').removeClass('.show').removeClass('modal-backdrop');
                    })



                    $('#btnAdd').on('click', function(){
                        $('#formTitle').val('')
                        $('#formDescription').val('');
                        $('.modal-backdrop').addClass('show');
                        $('#addReminders').addClass('show').css('display', 'block');
                   
                    });
                    $('#closeAddReminders').on('click', function(){
                        $('#addReminders').removeClass('show').css('display', 'none');
                        $('.modal-backdrop').removeClass('.show').removeClass('modal-backdrop');
                    })

                    $('#addRemindersSubmit').on('click',function (e) {
                        e.preventDefault();

                        $.ajax({
                            type: 'POST',
                            url: '/api/v1/reminders/insert',
                            data: {
                                title: $('#formTitle').val(),
                                description: $('#formDescription').val(),
                                event_at: $('#formEvent').val(),
                            },
                            success: function (response) {
                            if(response.ok === true){
                                window.location.href = '/reminders/list';
                            }
                            },

                            error: function(xhr, status, error) {
                            const data = JSON.parse(xhr.responseText);
                            console.log(data);
                            if(data.ok === false){
                                alert(JSON.stringify(data.msg))
                                $('#addReminders').removeClass('show').css('display', 'none');
                                $('.modal-backdrop').removeClass('.show').removeClass('modal-backdrop');
                            }
                            }
                        });
                    });

                    $('#editRemindersSubmit').on('click',function (e) {
                        e.preventDefault();

                        $.ajax({
                            type: 'PUT',
                            url: '/api/v1/reminders/update',
                            data: {
                                id: $('#formEditId').val(),
                                title: $('#formEditTitle').val(),
                                description: $('#formEditDescription').val(),
                                event_at: $('#formEditEvent').val(),
                            },
                            success: function (response) {
                            if(response.ok === true){
                                window.location.href = '/reminders/list';
                            }
                            },

                            error: function(xhr, status, error) {
                            const data = JSON.parse(xhr.responseText);
                            console.log(data);
                            if(data.ok === false){
                                alert(JSON.stringify(data.msg))
                                $('#addReminders').removeClass('show').css('display', 'none');
                                $('.modal-backdrop').removeClass('.show').removeClass('modal-backdrop');
                            }
                            }
                        });
                    });

                    $('#deleteRemindersSubmit').on('click',function (e) {
                        e.preventDefault();

                        $.ajax({
                            type: 'DELETE',
                            url: '/api/v1/reminders/delete',
                            data: {
                                id: $('#formDeleteId').val(),
                            },
                            success: function (response) {
                            if(response.ok === true){
                                window.location.href = '/reminders/list';
                            }
                            },

                            error: function(xhr, status, error) {
                            const data = JSON.parse(xhr.responseText);
                            console.log(data);
                            if(data.ok === false){
                                alert(JSON.stringify(data.msg))
                                $('#addReminders').removeClass('show').css('display', 'none');
                                $('.modal-backdrop').removeClass('.show').removeClass('modal-backdrop');
                            }
                            }
                        });
                    });




                } else {
                    // Handle the case where the response is not successful
                    console.error('Error fetching reminders data.');
                }
            },
            error: function (error) {
                // Handle the AJAX error
                console.error('AJAX error:', error);
            }
        });
    });
</script>

@endsection
