<?php
use Tests\TestCase;

uses(TestCase::class);
it('Has Auth Login Page', function () {
    $response = $this->get('/');

    $response->assertStatus(200);
});

it('Has Auth Register Page', function () {
    $response = $this->get('/register');

    $response->assertStatus(200);
});