# Automatest

Problem: Building a E-Wallet API.

Build an “E-Wallet” application where users can log in, deposit and transfer their balance to other users

## Authors

- [@wisnunugraha](https://www.gitlab.com/wisnunugraha)

## API Reference

#### Auth API Login

```http
  POST /api/v1/auth/login
```

| Parameter.  | Type     | Description                |
| :---------- | :------- | :------------------------- |
| `email` | `string` | **Required**. Your API key |
| `password`  | `string` | **Required**. Your API key |

#### Auth API Register

```http
  POST /api/v1/auth/register
```

| Parameter.  | Type     | Description                |
| :---------- | :------- | :------------------------- |
| `fname` | `string` | **Required**. Your API key |
| `lname` | `string` | **Required**. Your API key |
| `email` | `string` | **Required**. Your API key |
| `password`  | `string` | **Required**. Your API key |

## Run This Project Cloud

import postman

```bash
  file Postman Automa - Test.postman_collection.json
```



## Run This Project Localhost

run this project

```bash
  create db `walletdb`
```

clone this project and run npm install

```bash
  clone this project and cd to project
```

```bash
  npm install
```

you must check configurations on .env of database, port and etc.

```bash
  .env folder
```

before run project, you must migrations table, you need step by step for migrations

```bash
  npm install knex -g
```

```bash
  knex migrate:latest
```

run under development

```bash
  npm run dev
```

run under productions

```bash
  npm run start
```

import postman

```bash
  file Postman Wallet - Test.postman_collection.json
```
