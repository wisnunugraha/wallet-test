<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        // "id": 1,
        // "title": "Meeting with Bob",
        // "description": "Discuss about new project related to new system",
        // "remind_at": "1701246722",
        // "event_at": "1701223200"
        Schema::create('tokens', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('auths_id');
            $table->text('token');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tokens');
    }
};
