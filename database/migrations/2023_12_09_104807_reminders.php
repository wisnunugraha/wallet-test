<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
 * Run the migrations.
 */
public function up(): void
{
    // "id": 1,
    // "title": "Meeting with Bob",
    // "description": "Discuss about new project related to new system",
    // "remind_at": "1701246722",
    // "event_at": "1701223200"
    Schema::create('reminders', function (Blueprint $table) {
        $table->id();
        $table->string('title');
        $table->string('description');
        $table->integer('remind_at');
        $table->integer('event_at');
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
        $table->timestamp('deleted_at')->nullable();
    });
}

/**
 * Reverse the migrations.
 */
public function down(): void
{
    Schema::dropIfExists('reminders');
}
};
