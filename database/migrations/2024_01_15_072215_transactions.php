<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id')->index();
            $table->string('reference_id')->nullable();
            $table->decimal('balance_before', 65, 0)->default(0);
            $table->decimal('balance_after', 65, 0)->default(0);
            $table->decimal('credit', 65, 0)->default(0);
            $table->decimal('debit', 65, 0)->default(0);
            $table->date('date');
            $table->integer('type')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('auths');
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
